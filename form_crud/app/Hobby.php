<?php

namespace App;

use App\Student;
use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
    protected $fillable = ['name'];

    public function students(){
        
        return $this->belongsToMany(Student::class);
    }
}
