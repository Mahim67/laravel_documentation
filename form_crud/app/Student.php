<?php

namespace App;

use App\Hobby;
use App\Picture;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name','birth','gender','email','tel','color','country','about','image'
    ];

    public function hobbies()
    {
        return $this->belongsToMany(Hobby::class);
    }

    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }
}
