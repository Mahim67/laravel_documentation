<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = ['multipic'];
    
    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
