<?php

namespace App\Http\Controllers;

use App\Hobby;
use Illuminate\Http\Request;

class HobbyController extends Controller
{
    public function index(){
        $datas = Hobby::all();
        return view('hobby.index', compact('datas'));
    }
    public function create(){
        return view('hobby.create');
    }
    public function store(Request $request){
        Hobby::create($request->all());
        return redirect()->route('hobby.index');
    }
    public function edit($id){
        $hobby = Hobby::findOrFail($id);
        return view('hobby.edit', compact('hobby'));
    }
    public function update(Request $request, Hobby $hobby){
        $hobby->update($request->all());
        return redirect()->route('hobby.index');
    }
    public function destroy($id){
        Hobby::find($id)
        ->delete();
        return redirect()->route('hobby.index');
    }
}
