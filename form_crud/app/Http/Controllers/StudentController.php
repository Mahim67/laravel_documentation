<?php

namespace App\Http\Controllers;

use Image;
use App\Hobby;
use App\Picture;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class StudentController extends Controller
{
    public function index()
    {
     
     $datas = Student::all();
     return view('student.index', compact('datas'));
    }

    public function create()
    {
        $hobbies = Hobby::pluck('name','id')->toArray();
        $images = Picture::pluck('multipic','id')->toArray();
        $data = [];
        return view('student.create', compact('hobbies', 'data','images'));
    }

    public function store(Request $request)
    {
        try {
            // dd($request->multipic);
            $hobby = $request['hobby'];
            $data = $request->all();

            // siglepicture
            if ($request->hasFile('image'))
            {
                $data['image'] = $this->uploadImage($request->image, $request->title);
            }

            $student = Student::create($data);
            $student->hobbies()->attach($hobby);

            // multipicture
            if($request->hasFile('multipic')){
                foreach ($request->multipic as $pic){
                    $student->pictures()->create([
                    'multipic' => $this->uploadMultipleImage($pic),
               ]);
            }
        }
        return redirect()->route('student.index');
            } 
            catch (QueryException $e) {
                return view('student.create');
            }  
    }

    public function show(Student $student)
    {
        return view('student.show', compact('student'));   
    }

    public function edit(Student $student)
    {
        $hobbies = Hobby::pluck('name','id')->toArray();
        $data = $student->hobbies->pluck('id')->toArray();
        return view('student.edit', compact('student', 'hobbies','data'));
    }
    public function update(Request $request, Student $student)
    {
        $data = $request->all();
        if ($request->hasFile('image')) {
            $this->unlink($student->image);
            $data['image'] = $this->uploadImage($request->image, $request->title);
        }
        // multipicture
        if($request->hasFile('multipic')){
            foreach ($request->multipic as $pic){
                $student->pictures()->create([
                'multipic' => $this->uploadMultipleImage($pic),
                ]);
            }
        }
        $student->update($data);
        $hobby = $request->hobby;
        $student->hobbies()->sync($hobby);
        return redirect()->route('student.index');
    }

    public function destroy(Student $student)
    {
        $student->hobbies()->detach();
        $student->delete();
        return redirect()->back();
    }


    private function uploadImage($file, $name)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp .'-'.$name. '.' . $file->getClientOriginalExtension();
        $pathToUpload = storage_path().'/app/public/students/';
        Image::make($file)->resize(634,792)->save($pathToUpload.$file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        $pathToUpload = storage_path().'/app/public/students/';
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }
// multiple images

private function uploadMultipleImage($file)
{
    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
    $name = $file->getClientOriginalName();
    $file_name = $timestamp .'-' . $name;
    $pathToUpload = storage_path().'/app/public/students/';
    Image::make($file)->resize(634,792)->save($pathToUpload.$file_name);
    return $file_name;
}
    
   
}
