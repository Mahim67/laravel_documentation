@extends('layouts.app')
@section('content')
    <div class="card text-left">
      <div class="container">
          <a href="{{ route('student.index') }}" class="btn btn-success">List</a>
          {!! Form::model($student, [
              'route' => ['student.update',$student->id],
              'method' => 'PUT',
              'files' => true
          ]) !!}
          @include('student.form')
          {!! Form::submit('submit' ,['class' => 'btn btn-success ']) !!}
          {!! Form::close() !!}
      </div>
    </div>
@endsection