@extends('layouts.app')
@section('content')
    <div class="card mt-5">
        <div class="container">
            <h2>Students Register Form</h2>
            <a href="{{ route('student.index') }}" class="btn btn-success">List</a>
            <hr>
            {!! Form::open([
            'route' => 'student.store',
            'method' => 'POST',
            'enctype' => 'multipart/form-data',
            ]) !!}
            @include('student.form')
            {!! Form::submit('submit', ['class' => 'btn btn-primary mb-1']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
