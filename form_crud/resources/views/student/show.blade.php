@extends('layouts.app')
@section('content')
    <div class="card text-left mt-5">
        <div class="container">
            <h3><strong>{{ $student->name }}</strong> Information</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Name</th>
                        <td>{{ $student->name }}</td>
                    </tr>
                    <tr>
                        <th>Birthday</th>
                        <td>{{ $student->birth }}</td>
                    </tr>
                    <tr>
                        <th>E-mail</th>
                        <td>{{ $student->email }}</td>
                    </tr>
                    <tr>
                        <th>Tel Phone</th>
                        <td>{{ $student->tel }}</td>
                    </tr>
                    <tr>
                        <th>Country</th>
                        <td>{{ $student->country }}</td>
                    </tr>
                    <tr>
                        <th>Colour</th>
                        <td>{{ $student->color }}</td>
                    </tr>
                    <tr>
                        <th>About</th>
                        <td>{{ $student->about }}</td>
                    </tr>
                    <tr>
                        <th>Hobbies</th>
                        <td>
                            @if (isset($student->hobbies))
                                <ul>
                                    @foreach ($student->hobbies as $hobby)
                                        <li>{{ $hobby->name }}</li>
                                    @endforeach
                                </ul>
                            @else
                                <div class="alert alert-info" role="alert">
                                    found no hobby
                                </div>
                            @endif

                        </td>
                    </tr>
                    <tr>
                        <th>Picture</th>
                        <td>
                            @if (file_exists(storage_path() . '/app/public/students/' . $student->image) && !is_null($student->image))
                                <img src="{{ asset('storage/students/' . $student->image) }}" height="100">
                            @else
                                {{-- <img src="{{ asset('/default-avatar.png') }}"
                                    --}} No Photo @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Multiple Picture</th>
                        <td>
                            @if (isset($student->pictures))
                                <ul>
                                    @foreach ($student->pictures as $item)
                                        @if (file_exists(storage_path() . '/app/public/students/' . $item->multipic) && !is_null($item->multipic))
                                            <img src="{{ asset('storage/students/' . $item->multipic) }}" height="100">
                                        @endif
                                    @endforeach
                                </ul>
                            @else
                                <div class="alert alert-info" role="alert">
                                    Found no Picture
                                </div>
                            @endif

                        </td>
                    </tr>

                </tbody>
            </table>

            <a href="{{ route('student.index') }}" class="btn btn-success mb-2">Student List</a>
        </div>
    </div>
@endsection
