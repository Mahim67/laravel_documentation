@extends('layouts.app')
@section('content')
    <div class="card text-left mt-5">
        <div class="container">
            <h3>Student Information</h3>
            <a href="{{ route('student.create') }}" class="btn btn-success mb-2">Create</a>
            <table class="table">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Birthday</th>
                        <th>Tel-phone</th>
                        <th>Country</th>
                        <th>Picture</th>
                        <th>Hobby</th>
                        <th >Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $sl = 1 ?> 
                    @foreach ($datas as $data)
                        <tr>
                            <td>{{ $sl++ }}</td>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->email }}</td>
                            <td>{{ $data->birth }}</td>
                            <td>{{ $data->tel }}</td>
                            <td>{{ $data->country }}</td>
                            <td>
                                @if(file_exists(storage_path().'/app/public/students/'.$data->image ) && (!is_null($data->image)))
                                    <img src="{{ asset('storage/students/'.$data->image) }}" height="100">
                                @else
                                    {{-- <img src="{{ asset('/default-avatar.png') }}" --}}
                                    No Photo
                            @endif
                            </td>
                            <td>
                                <ul>
                                    @foreach ($data->hobbies as $hobby)
                                        <li>{{ $hobby->name }}</li>
                                    @endforeach
                                </ul>
                            </td>
                            <td>
                                <a href="{{ route('student.show',$data->id) }}" class="btn btn-primary">show</a>
                                <a href="{{ route('student.edit',$data->id) }}" class="btn btn-warning">edit</a>
                                <form action="{{ route('student.destroy', $data->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are You Sure To Delete?')">delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
