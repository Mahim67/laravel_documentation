<div class="form-group row">
    {!! Form::label('name', 'Student Name :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('birth', 'Birthday (date and time) :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::date('birth', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('gender', 'Gender :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::radio('gender', 'male', false, ['male' => 'Male']) !!}
        {!! Form::label('male', null, []) !!}
        {!! Form::radio('gender', 'female', false, ['female' => 'Female']) !!}
        {!! Form::label('female', null, []) !!}
        {!! Form::radio('gender', 'other', false, ['others' => 'Others']) !!}
        {!! Form::label('others', null, []) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('email', 'E-mail :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('tel', 'Tel-phone :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::tel('tel', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('color', 'Your Favourite Color :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::color('color', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('country', 'Select Country :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::select('country', ['bangladseh' => 'Bangladseh', 'india' => 'India', 'pakistan' => 'pakistan'], null,
        ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('hobby', 'Hobbies :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        @foreach ($hobbies as $key=>$hobby)
            {!! Form::checkbox('hobby[]', $key, in_array($key,$data)) !!} {{ $hobby }}
        @endforeach
    </div>
</div>
<div class="form-group row">
    {!! Form::label('about', 'About Yourself :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::textarea('about', null, ['class' => 'form-control', 'rows' => '4']) !!}
    </div>
</div>
{{-- image --}}
<div class="form-group row">
    {!! Form::label('image', 'Picture :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::file('image', null, ['class'=>'col-sm-8']) !!}
    </div>
</div>
{{-- multiple image --}}
<div class="form-group row">
    {!! Form::label('multipic[]', 'Multiple Picture :', null, ['class' => 'col-sm-4']) !!}
    <div class="col-sm-8">
        <input type="file" name="multipic[]" class="col-sm-8" multiple>
    </div>
</div>
