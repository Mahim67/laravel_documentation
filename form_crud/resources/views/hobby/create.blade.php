@extends('layouts.app')
@section('content')
    <div class="card text-left mt-5">
        <div class="container">
            <h2>Create Hobby</h2><hr>
            {!! Form::open([
                'route' => 'hobby.store',
                'method' => 'POST'
            ]) !!}
            @include('hobby.form')
            {!! Form::submit('submit', ['class'=>'btn btn-primary mb-1']) !!}
            {{-- {!! Form::close() !!} --}}
        </div>
    </div>
@endsection