@extends('layouts.app')
@section('content')
    <div class="card text-left mt-5">
        <div class="container">
            <h2>Update Hobby</h2><hr>
            {!! Form::model($hobby,[
                'route' => ['hobby.update',$hobby->id],
                'method' => 'PUT'
            ]) !!}
            @include('hobby.form')
            {!! Form::submit('submit', ['class'=>'btn btn-primary mb-1']) !!}
            {{-- {!! Form::close() !!} --}}
        </div>
    </div>
@endsection