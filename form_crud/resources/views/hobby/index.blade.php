@extends('layouts.app')
@section('content')
    <div class="card text-left mt-5">
        <div class="container">
            <h3>Student Information</h3>
            <a href="{{ route('hobby.create') }}" class="btn btn-success mb-2">Create</a>
            <table class="table">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $sl = 1 ?>
                    @foreach ($datas as $data)
                        <tr>
                            <td>{{ $sl++ }}</td>
                            <td>{{ $data->name }}</td>
                            <td>
                                <a href="{{ route('hobby.edit',$data->id) }}" class="btn btn-warning">edit</a>
                                {!! Form::open([
                                    'route' => ['hobby.destroy',$data->id],
                                    'method' => 'DELETE',
                                    'style' => 'display: inline'
                                    ]) !!}
                                    <button class="btn btn-icon waves-effect btn-danger" type="submit"
                                        onclick="return confirm('Are You Sure To Delete This')">
                                        Delete </button>
                                    {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
