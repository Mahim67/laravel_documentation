@extends('post.layouts.app')
@section('content')

    <div class="card mt-5">
        <div class="container mt-2">
            <a name="" id="" class="btn btn-success" href="{{ route('post.index') }}" role="button">Post List</a>
            <table class="table mt-1">
                <thead>
                    <tr>
                        <th width="30%">Index</th>
                        <th>Value</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <th>Title </th>
                        <td>{{ $post->title }}</td>
                    </tr>
                    <tr>
                        <th>Body</th>
                        <td>{{ $post->body }}</td>
                    </tr>
                    <tr>
                        <td>
                            @if (count($comments) > 0)
                                <p>Comments <span class="badge badge-dark">{{ count($comments) }}</span></p>
                            @else
                                <h3>Comment</h3>
                            @endif
                        </td>
                        <td>
                            <ul>
                                @foreach ($comments as $comment)
                                    <li>{{ $comment->body }}</li>
                                @endforeach
                                {{-- @forelse ($comments as $comment)
                                    <li>{{ $comment->body }}</li>
                                @empty
                                    <p>No comment</p>
                                @endforelse --}}
                            </ul>
                        </td>
                    </tr>

                </tbody>
            </table>
            {!! Form::open([
            'method' => 'POST',
            'route' => ['comment.store', $post->id],
            'class' => 'form-horizontal',
            ]) !!}
            <div class="form-group row pull-right{{ $errors->has('comment') ? ' has-error' : '' }}">
                {!! Form::label('comment', 'Comment :', ['class' => 'col-sm-4']) !!}
                {!! Form::textarea('comment', null, ['class' => 'col-sm-8 form-control', 'rows' => '4', 'required' =>
                'required']) !!}
                <small class="text-danger">{{ $errors->first('comment') }}</small>

                {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
