@extends('post.layouts.app')
@section('content')

    <div class="card mt-5">
        <div class="container mt-2">
            <a name="" id="" class="btn btn-success" href="{{ route('post.index') }}" role="button">Post List</a>
            {!! Form::model($post,[
                'method' => 'PUT',
                'route' => ['post.update', $post->id],
                'class' => 'form-horizontal mb-2']
                ) !!}
            @include('post.form')

            <div class="btn-group pull-right">
            {!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
