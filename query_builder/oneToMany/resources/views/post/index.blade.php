@extends('post.layouts.app')
@section('content')

    <div class="card mt-5">
        <div class="container mt-2">
            <a name="" id="" class="btn btn-success" href="{{ route('post.create') }}" role="button">Create Post</a>
            <table class="table mt-1">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td scope="row">{{ $post->title }}</td>
                            <td>{{ $post->body }}</td>
                            <td>
                                <a name="" id="" class="btn btn-primary" href="{{ route('post.show', $post->id) }}"
                                    role="button">Show</a>
                                <a name="" id="" class="btn btn-warning" href="{{ route('post.edit', $post->id) }}"
                                    role="button">Edit</a>
                                {!! Form::open([
                                'route' => ['post.destroy', $post->id],
                                'method' => 'DELETE',
                                'style' => 'display: inline',
                                ]) !!}
                                <button class="btn btn-icon waves-effect btn-danger" type="submit"
                                    onclick="return confirm('Are You Sure To Delete This?')">
                                    Delete </button>
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $posts->links() }}
        </div>
    </div>
@endsection
