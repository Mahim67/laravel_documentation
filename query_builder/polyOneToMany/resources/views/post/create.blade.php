@extends('layouts.app')
@section('content')
<div class="card p-3">
    <div class="container">
        {!! Form::open([
        'route' => 'post.store',
        'method' => 'post'
        ]) !!}
        @include('post.form')

        {!! Form::submit('Submit', [
            'class' => 'btn btn-success'
        ]) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection
