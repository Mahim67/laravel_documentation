<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Post;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CommentController extends Controller
{
    public function store(Request $request,$id){
        DB::table('comments')
        ->insert([
            'commentable_type' => 'posts',
            'commentable_id' => $id,
            'body' => $request->comment,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        return redirect()->back();
    }
}
