@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <br><h3>Category List</h3>
    <a href="{{ route('category.create') }}" class="btn btn-success">Create Category</a>
    

    <table class="table table-hover mt-1">
        <thead>
            <tr>
                <th scope="col">SL</th>
                <th scope="col">title</th>
                <th scope="col">created</th>
                <th scope="col" class="row">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $sl = 1 ?>
            @foreach ($categories as $category)
            <tr>
            <th scope="row">{{ $sl++ }}</th>
                <td>{{$category->title}}</td>
                <td>{{$category->created_at}}</td>
                <td class="row">
                    <a href="{{ route('category.edit', $category->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('category.destroy',$category->id) }}" method="post" class="">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection