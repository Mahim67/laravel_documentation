@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <br><h3>Product List</h3>
    <a href="{{ route('product.create') }}" class="btn btn-success">Create Product</a>

    <table class="table table-hover mt-1">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">Category</th>
                <th scope="col">Description</th>
                <th scope="col">Prize</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $sl = 1 ?>
            @foreach ($products as $product)
            <tr>
            <th scope="row">{{ $sl++ }}</th>
                <td>{{$product->title}}</td>
                <td>{{ $product->category->title }}</td>
                <td>{{ $product->description}}</td>
                <td>{{ $product->prize}}</td>
                <td class="row">
                    <a href="{{ route('product.show', $product->id) }}" class="btn btn-primary">Show</a>
                    <a href="{{ route('product.edit',$product->id) }}" class="btn btn-success">Edit</a>
                    <form action="{{ route('product.destroy',$product->id) }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection