@extends('layouts.app')

@section('content')

<div class="container">
    <h4>Create Product</h4><br><br>
    <a href="{{ route('product.index') }}" class="btn btn-primary">Back</a>

    {!! Form::open
    (['route' => 'product.store',
    'method' => 'POST'])
    !!}

    <div class="form-group row">
        {!! Form::label('title','Title :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('title','', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('category_id','Category :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            <select class="custom-select" id="inputGroupSelect01" name="category_id">
                <option selected>Choose One Category ...</option>
                @foreach ($categories as $key => $category)
                <option value="{{ $key }}">{{ $category }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('description','Description :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::textarea('description','', ['class'=>'form-control' ,'rows'=>'3']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('prize','Prize :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('prize','', ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::submit('submit', ['class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}



</div>

@endsection