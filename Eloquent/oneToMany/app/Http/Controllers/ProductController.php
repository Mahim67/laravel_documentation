<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('product.index', compact('products'));
    }

    public function create()
    {
        
        $categories = Category::pluck('title','id')->toArray();
        return view('product.create', compact('categories'));
    }

    public function store(Request $request)
    {    
        $data = $request->all();
        $products = Product::create($data);
        return redirect()->route('product.index');     
    }

    public function show($id)
    {
        $products = Product::findOrFail($id);
        $categories = Category::pluck('title','id');
        return view('product.show',compact('products','categories'));
    }

    public function edit($id)
    {
        $product = Product::findOrfail($id);
        $categories = Category::pluck('title','id');
        return view('product.edit',compact('product','categories'));
    }

    public function update(Request $request, Product $product)
    {
        $data = $request->all();
        // dd($data);
        $product->update($data);
        return redirect()->route('product.index');
    }

    public function destroy($id)
    {
        $product = Product::findOrfail($id)->delete();
        
        return redirect()->route('product.index');
    }
}
