@extends('layouts.app')
@section('content')
<div class="card p-3">
    <div class="container">
        <div class="col-sm-2 offset-12 mb-2">
            <a href="{{ route('post.create') }}" class="btn btn-success">Create</a>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Action</th>
                </tr>
            </thead>
            @foreach ($posts as $post)
            <tr>
                <td>{{$post->title}}</td>
                <td>{{$post->body}}</td>
                <td>
                    <a href="{{ route('post.show',$post->id) }}" class="btn btn-info">show</a>
                    <a href="{{ route('post.edit',$post->id) }}" class="btn btn-warning">edit</a>
                    {!! Form::open([
                    'route' => ['post.destroy',$post->id],
                    'method' => 'DELETE',
                    'style' => 'display: inline'
                    ]) !!}
                    <button class="btn btn-icon waves-effect btn-danger" type="submit"
                        onclick="return confirm('Are You Sure To Delete This')">
                        Delete </button>
                    {!! Form::close() !!}

                </td>
            </tr>
            @endforeach
        </table>
        <div class="row">  
			<div class="col-sm-12">
				{!! $posts->links() !!}
			</div>
		</div>
    </div>
</div>
@endsection
