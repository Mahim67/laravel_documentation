@extends('layouts.app')
@section('content')
<div class="card p-3">
    <div class="container">
        {!! Form::model($post,[
        'route' => ['post.update',$post->id],
        'method' => 'put'
        ]) !!}
        @include('post.form')

        {!! Form::submit('Update', [
            'class' => 'btn btn-success'
        ]) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection