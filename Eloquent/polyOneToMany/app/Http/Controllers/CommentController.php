<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request, Post $post,$id){
        $post = Post::find($id);
        $post->comments()->create([
            'body' => $request->comment,
        ]);
        return redirect()->back();
    }
}
