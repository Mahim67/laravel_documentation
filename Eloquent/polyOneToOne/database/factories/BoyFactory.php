<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Boy;
use App\Model;
use App\Mobile;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Boy::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
$factory->define(App\Mobile::class, function (Faker $faker) {
    return [
        'mobile' => $faker->word
    ];
});
