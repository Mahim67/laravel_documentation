<?php

use App\Boy;
use App\Mobile;
use Illuminate\Database\Seeder;

class BoyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boy = factory(App\Boy::class)->create();
        $boy->mobile()->save(factory(App\Mobile::class)->make());
    }
}
