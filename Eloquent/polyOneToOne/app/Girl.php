<?php

namespace App;

use App\Mobile;
use Illuminate\Database\Eloquent\Model;

class Girl extends Model
{
    protected $fillable = ['name'];

    public function mobile()
    {
        return $this->morphOne(Mobile::class,'mobileable');
    }
}
