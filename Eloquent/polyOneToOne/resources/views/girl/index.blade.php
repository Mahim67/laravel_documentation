@extends('layouts.app')
@section('content')
<div class="card p-3">
    <div class="col-sm-2 offset-10 mb-2">
        <a href="{{ route('girl.create') }}" class="btn btn-success">create</a>
    </div>

    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Actions</th>
                </tr>
            </thead>
            @foreach ($girls as $girl)
            <tr>
                <td>{{$girl->name}}</td>
                <td>{{$girl->mobile->mobile}}</td>
                <td>
                    <a href="{{ route('girl.show',$girl->id) }}" class="btn btn-info">show</a>
                    <a href="{{ route('girl.edit',$girl->id) }}" class="btn btn-warning">edit</a>
                    {!! Form::open([
                    'route' => ['girl.destroy',$girl->id],
                    'method' => 'DELETE',
                    'style' => 'display: inline'
                    ]) !!}
                    <button class="btn btn-icon waves-effect btn-danger" type="submit"
                        onclick="return confirm('Are You Sure To Delete This')">
                        Delete </button>
                    {!! Form::close() !!}

                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection