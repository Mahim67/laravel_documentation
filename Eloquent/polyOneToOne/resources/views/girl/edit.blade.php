@extends('layouts.app')
@section('content')
<div class="card p-3">
    <div class="container">
        {!! Form::model($girl,[
        'route' => ['girl.update',$girl->id],
        'method' => 'put'
        ]) !!}
        @include('girl.form')

        {!! Form::submit('Update', [
            'class' => 'btn btn-success'
        ]) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection