<div class="form-group">
{!! Form::label('name', "Name : ") !!}
{!! Form::text('name', null, [
    'class' => 'form-control'
]) !!}
</div>
<div class="form-group">
{!! Form::label('mobile', "Mobile : ") !!}
{!! Form::text('mobile', isset($boy->mobile) ? $boy->mobile->mobile : null, [
    'class' => 'form-control'
]) !!}
</div>