@extends('layouts.app')
@section('content')
<div class="card p-3">
    <div class="container">
        {!! Form::model($boy,[
        'route' => ['boy.update',$boy->id],
        'method' => 'put'
        ]) !!}
        @include('boy.form')

        {!! Form::submit('Update', [
            'class' => 'btn btn-success'
        ]) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection