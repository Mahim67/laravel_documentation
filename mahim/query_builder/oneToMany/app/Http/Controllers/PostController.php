<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function index(){
        $posts = DB::table('posts')
        ->paginate(3);
        return view('post.index', compact('posts'));
    }
    public function create(){
        return view('post.create');
    }
    public function store(Request $request){
        DB::table('posts')
        ->insert([
            'title' => $request->title,
            'body' => $request->body,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        return redirect()->route('post.index'); 
    }
    public function show($id){
        $post = DB::table('posts')
        ->where('id', $id)
        ->first();
        $comments = DB::table('posts')->where('posts.id', $id)
        ->leftJoin('comments', 'posts.id', '=', 'comments.post_id')
        ->select('comments.body')
        ->get();
        return view('post.show', compact('post', 'comments'));
    }
    public function edit($id)
    {
        $post = DB::table('posts')->where('id', $id)->first();
        return view('post.edit', compact('post'));
    }

    public function update(Request $request, $id)
    {
        DB::table('posts')->where('id', $id)->update([
            'title' => $request->title,
            'body' => $request->body,
        ]);
        return redirect(route('post.index'));
    }

    public function destroy($id)
    {
        $post = DB::table('posts')->where('id', $id)->delete();
        return redirect(route('post.index'));
    }

}
