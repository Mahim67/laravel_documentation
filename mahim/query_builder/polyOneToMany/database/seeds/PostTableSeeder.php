<?php

use App\Post;
use App\Comment;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    public function run()
    {
        $post = factory(App\Post::class)->create();
        $post->comments()->save(factory(App\Comment::class)->make());
    }
}
