@extends('layouts.app')
@section('content')
<div class="card p-3">
    <div class="container">
        <div class="col-sm-2 offset-12 mb-2">
            <a href="{{ route('post.index') }}" class="btn btn-success">Post List</a>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th width="30%">Index</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Title </td>
                    <td>{{ $post->title }}</td>
                </tr>
                <tr>
                    <td>Body </td>
                    <td>{{ $post->body }}</td>
                </tr>
                <tr>
                   <td> @if (count($post->comments) > 0)
                    <p>Comments <span class="badge badge-dark">{{count($post->comments)}}</span></p>
                    @else
                    <h3>Comment</h3>
                @endif
            </td>
            <td>
                <ul>
                    @foreach ($post->comments as $comment)
                    <li>{{$comment->body}}</li>
                    @endforeach
                </ul>
            </td>
                </tr>
            </tbody>
        </table>
        <form action="{{ route('post.comment', $post->id) }}" method="POST">
            @csrf
            <div class="form-gourp row">
                <label for="comment" class="col-sm-4 col-form-label">Comment</label>
                <div class="col-sm-8">
                    <textarea name="comment" id="comment" rows="3"></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection