<div class="form-group">
    {!! Form::label('title', "Title : ", null) !!}
    {!! Form::text('title', null, [
        'class' => 'form-control'
    ]) !!}
    </div>
    <div class="form-group">
    {!! Form::label('body', "Body : ", null) !!}
    {!! Form::text('body', null, [
        'class' => 'form-control'
    ]) !!}
    </div>