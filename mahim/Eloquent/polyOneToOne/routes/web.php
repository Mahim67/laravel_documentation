<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/boy', function () {
    return view('boy.index');
});

Route::resource('boy','BoyController');
Route::get('trash/boy','BoyController@trashList')->name('boy.trash');
Route::put('trash/boy/{id}/restore','BoyController@restore')->name('boy.restore');
Route::delete('trash/boy/{id}/delete','BoyController@permanentDelete')->name('boy.delete');
Route::resource('girl','GirlController');
Route::resource('mobile','MobileController');