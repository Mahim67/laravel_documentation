@extends('layouts.app')
@section('content')
<div class="card p-3">

    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Actions</th>
                </tr>
            </thead>
            @foreach ($boys as $boy)
            <tr>
                <td>{{$boy->name}}</td>
                <td>{{$boy->mobile->mobile}}</td>
                <td>
                    {!! Form::open([
                    'route' => ['boy.restore',$boy->id],
                    'method' => 'put',
                    'style' => 'display: inline'
                    ]) !!}
                    <button class="btn btn-icon waves-effect btn-info" type="submit">
                        Restore </button>
                    {!! Form::close() !!}

                    {!! Form::open([
                    'route' => ['boy.delete',$boy->id],
                    'method' => 'DELETE',
                    'style' => 'display: inline'
                    ]) !!}
                    <button class="btn btn-icon waves-effect btn-danger" type="submit">
                        Delete </button>
                    {!! Form::close() !!}

                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection