@extends('layouts.app')
@section('content')
    <div class="card mt-5">
        <div class="container mt-3">
            <div class="row mb-1">
                <div class="col-sm-6 text-left">
                    <a href="{{ route('boy.create') }}" class="btn btn-success">create</a>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('boy.trash') }}" class="btn btn-dark">Trash List</a>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                @foreach ($boys as $boy)
                    <tr>
                        <td>{{ $boy->name }}</td>
                        <td>{{ $boy->mobile->mobile }}</td>
                        <td>
                            <a href="{{ route('boy.show', $boy->id) }}" class="btn btn-info">show</a>
                            <a href="{{ route('boy.edit', $boy->id) }}" class="btn btn-warning">edit</a>
                            {!! Form::open([
                            'route' => ['boy.destroy', $boy->id],
                            'method' => 'DELETE',
                            'style' => 'display: inline',
                            ]) !!}
                            <button class="btn btn-icon waves-effect btn-danger" type="submit"
                                onclick="return confirm('Are You Sure To Delete This')">
                                Delete </button>
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $boys->links() }}
        </div>
    </div>
@endsection
