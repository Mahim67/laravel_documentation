@extends('layouts.app')
@section('content')
<div class="card p-3">

    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th>Mobile</th>
                    <th>Name</th>
                </tr>
            </thead>
            @foreach ($mobiles as $mobile)
            <tr>
                <td>{{$mobile->mobile}}</td>
                <td>{{$mobile->mobileable->name}}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
@section('nav')
@include('orm.polyOnetoOne.navbar')
@endsection