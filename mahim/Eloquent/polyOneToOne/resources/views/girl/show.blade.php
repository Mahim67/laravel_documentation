@extends('layouts.app')
@section('content')
<div class="card p-3">
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th width="30%">Index</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Name </td>
                    <td>{{ $girl->name }}</td>
                </tr>
                <tr>
                    <td>Many car Owner </td>
                    <td>{{ $girl->manyArticles }}</td>
                </tr>
                <tr>
                    <td>Mobile </td>
                    <td>{{ $girl->mobile->mobile }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection