<?php

namespace App;

use App\Mobile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Boy extends Model
{
    protected $fillable = ['name'];
    use SoftDeletes;
    public function mobile()
    {
        return $this->morphOne(Mobile::class,'mobileable');
    }
}
