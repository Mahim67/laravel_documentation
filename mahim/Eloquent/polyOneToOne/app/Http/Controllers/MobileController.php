<?php

namespace App\Http\Controllers;

use App\Mobile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MobileController extends Controller
{
    public function index()
    {
        $mobiles = Mobile::all();
        return view('mobile.index', compact('mobiles'));
    }
}
