<?php

namespace App\Http\Controllers;

use App\Girl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GirlController extends Controller
{
    public function index()
    {
        $girls = Girl::all();
        return view('girl.index', compact('girls'));
    }
    public function create()
    {
        return view('girl.create');
    }
    public function store(Request $request, Girl $girl)
    {
        $girl = Girl::create($request->all());
        $girl->mobile()->create([
            'mobile' => $request->mobile
        ]);
        return redirect(route('girl.index'));
    }
    public function show(Girl $girl)
    {
        return view('girl.show',compact('girl'));
    }
    public function edit(Girl $girl)
    {
        return view('girl.edit',compact('girl'));
    }
    public function update(Request $request, Girl $girl)
    {
        $girl->update($request->all());
        $girl->mobile()->create([
            'mobile' => $request->mobile
        ]);
        return redirect(route('girl.index'));
    }
    public function destroy(Girl $girl)
    {
        $girl->delete($girl);
        return redirect(route('girl.index'));
    }
}
