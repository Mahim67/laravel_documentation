<?php

namespace App\Http\Controllers;

use App\Boy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BoyController extends Controller
{
    public function index()
    {
        $boys = Boy::paginate(3);
        return view('boy.index', compact('boys'));
    }
    public function create()
    {
        return view('boy.create');
    }
    public function store(Request $request, Boy $boy)
    {
        $boy = Boy::create($request->all());
        $boy->mobile()->create([
            'mobile' => $request->mobile
        ]);
        return redirect(route('boy.index'));
    }
    public function show(Boy $boy)
    {
        return view('boy.show', compact('boy'));
    }
    public function edit(Boy $boy)
    {
       
        return view('boy.edit', compact('boy'));
    }
    public function update(Request $request, Boy $boy)
    {
        $boy->update($request->all());
        $boy->mobile()->update([
            'mobile' => $request->mobile
        ]);
        return redirect(route('boy.index'));
    }
    public function destroy(Boy $boy)
    {
        // $boy->mobile()->delete();
        $boy->delete($boy);
        return redirect(route('boy.index'));
    }
    public function trashList()
    {
        $boys = Boy::with('mobile')->onlyTrashed()->get();
        return view('boy.trashList', compact('boys'));
    }
    public function restore($id)
    {
        Boy::withTrashed()
            ->where('id', $id)
            ->restore();
        return redirect()->back();
    }
    public function permanentDelete($id)
    {
        $boy =  Boy::withTrashed()
            ->where('id', $id)->first();
        $boy->mobile()->delete();
        $boy->forceDelete();
        return redirect()->back();
    }
}
